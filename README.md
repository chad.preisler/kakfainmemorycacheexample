# Kafka In Memory Cache Transformer Example.

# License

Copyright 2021 Chad Preisler.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

# Description

This code shows how to handle cyclic relationships in Kafka using the stream
DSL and processor API. Specifically this allows a stream to KTable join where
the topic for the KTable is the same as the topic that the stream outputs too.

# How to Build

This is a standard maven project. Build with:

 ```
maven clean package
```

# How to Run

You will need to have zookeeper and kafka running on your local machine for this
to work. See the [Kafka Quick Start](https://kafka.apache.org/quickstart) for instructions.

After you have Kafka running create the topics using the `create_topics.sh` shell script. Then run
the following shell scripts in any order.

1. Open a terminal and run `consumer.sh`.
2. Open a terminal and run `producer.sh`.
3. Open a terminal and run `stream.sh`.

You should see output in `consumer.sh` that looks like the following.

```
offset: 0, key = A, value = value-1a, null
offset: 1, key = B, value = value-1b, null
offset: 2, key = A, value = value-2a, value-1a, null
offset: 3, key = B, value = value-2b, value-1b, null
offset: 4, key = A, value = value-3a, value-2a, value-1a, null
```

There should also be log messages in the stream application that look something like
the following.

```
[inMemoryStreamApp-f6457a1f-25a7-4fb7-a60e-0f32e45e9158-StreamThread-1] INFO chad.preisler.kafka.inmemorycache.store.CachingTransformer - In transformer checking cache for right side.
[inMemoryStreamApp-f6457a1f-25a7-4fb7-a60e-0f32e45e9158-StreamThread-1] INFO chad.preisler.kafka.inmemorycache.store.CachingTransformer - Didn't find it in the cache. Checking KTable.
[inMemoryStreamApp-f6457a1f-25a7-4fb7-a60e-0f32e45e9158-StreamThread-1] INFO chad.preisler.kafka.inmemorycache.store.CachingTransformer - Executing the join logic...
[inMemoryStreamApp-f6457a1f-25a7-4fb7-a60e-0f32e45e9158-StreamThread-1] INFO chad.preisler.kafka.inmemorycache.store.CachingTransformer - In transformer checking cache for right side.
[inMemoryStreamApp-f6457a1f-25a7-4fb7-a60e-0f32e45e9158-StreamThread-1] INFO chad.preisler.kafka.inmemorycache.store.CachingTransformer - Didn't find it in the cache. Checking KTable.
[inMemoryStreamApp-f6457a1f-25a7-4fb7-a60e-0f32e45e9158-StreamThread-1] INFO chad.preisler.kafka.inmemorycache.store.CachingTransformer - Executing the join logic...
[inMemoryStreamApp-f6457a1f-25a7-4fb7-a60e-0f32e45e9158-StreamThread-1] INFO chad.preisler.kafka.inmemorycache.store.CachingTransformer - In transformer checking cache for right side.
[inMemoryStreamApp-f6457a1f-25a7-4fb7-a60e-0f32e45e9158-StreamThread-1] INFO chad.preisler.kafka.inmemorycache.store.CachingTransformer - Executing the join logic...
[inMemoryStreamApp-f6457a1f-25a7-4fb7-a60e-0f32e45e9158-StreamThread-1] INFO chad.preisler.kafka.inmemorycache.store.CachingTransformer - In transformer checking cache for right side.
[inMemoryStreamApp-f6457a1f-25a7-4fb7-a60e-0f32e45e9158-StreamThread-1] INFO chad.preisler.kafka.inmemorycache.store.CachingTransformer - Executing the join logic...
[inMemoryStreamApp-f6457a1f-25a7-4fb7-a60e-0f32e45e9158-StreamThread-1] INFO chad.preisler.kafka.inmemorycache.store.CachingTransformer - In transformer checking cache for right side.
[inMemoryStreamApp-f6457a1f-25a7-4fb7-a60e-0f32e45e9158-StreamThread-1] INFO chad.preisler.kafka.inmemorycache.store.CachingTransformer - Executing the join logic...
```

If you shutdown the sream application run it again and run the producer again the 
stream will initially find the existing record in the KTable and the 
console logs should look something like this.

```
[inMemoryStreamApp-f6457a1f-25a7-4fb7-a60e-0f32e45e9158-StreamThread-1] INFO chad.preisler.kafka.inmemorycache.store.CachingTransformer - In transformer checking cache for right side.
[inMemoryStreamApp-f6457a1f-25a7-4fb7-a60e-0f32e45e9158-StreamThread-1] INFO chad.preisler.kafka.inmemorycache.store.CachingTransformer - Didn't find it in the cache. Checking KTable.
[inMemoryStreamApp-f6457a1f-25a7-4fb7-a60e-0f32e45e9158-StreamThread-1] INFO chad.preisler.kafka.inmemorycache.store.CachingTransformer - Found the value in the KTable.
[inMemoryStreamApp-f6457a1f-25a7-4fb7-a60e-0f32e45e9158-StreamThread-1] INFO chad.preisler.kafka.inmemorycache.store.CachingTransformer - Executing the join logic...
[inMemoryStreamApp-f6457a1f-25a7-4fb7-a60e-0f32e45e9158-StreamThread-1] INFO chad.preisler.kafka.inmemorycache.store.CachingTransformer - In transformer checking cache for right side.
[inMemoryStreamApp-f6457a1f-25a7-4fb7-a60e-0f32e45e9158-StreamThread-1] INFO chad.preisler.kafka.inmemorycache.store.CachingTransformer - Didn't find it in the cache. Checking KTable.
[inMemoryStreamApp-f6457a1f-25a7-4fb7-a60e-0f32e45e9158-StreamThread-1] INFO chad.preisler.kafka.inmemorycache.store.CachingTransformer - Found the value in the KTable.
[inMemoryStreamApp-f6457a1f-25a7-4fb7-a60e-0f32e45e9158-StreamThread-1] INFO chad.preisler.kafka.inmemorycache.store.CachingTransformer - Executing the join logic...
[inMemoryStreamApp-f6457a1f-25a7-4fb7-a60e-0f32e45e9158-StreamThread-1] INFO chad.preisler.kafka.inmemorycache.store.CachingTransformer - In transformer checking cache for right side.
[inMemoryStreamApp-f6457a1f-25a7-4fb7-a60e-0f32e45e9158-StreamThread-1] INFO chad.preisler.kafka.inmemorycache.store.CachingTransformer - Executing the join logic...
[inMemoryStreamApp-f6457a1f-25a7-4fb7-a60e-0f32e45e9158-StreamThread-1] INFO chad.preisler.kafka.inmemorycache.store.CachingTransformer - In transformer checking cache for right side.
[inMemoryStreamApp-f6457a1f-25a7-4fb7-a60e-0f32e45e9158-StreamThread-1] INFO chad.preisler.kafka.inmemorycache.store.CachingTransformer - Executing the join logic...
[inMemoryStreamApp-f6457a1f-25a7-4fb7-a60e-0f32e45e9158-StreamThread-1] INFO chad.preisler.kafka.inmemorycache.store.CachingTransformer - In transformer checking cache for right side.
[inMemoryStreamApp-f6457a1f-25a7-4fb7-a60e-0f32e45e9158-StreamThread-1] INFO chad.preisler.kafka.inmemorycache.store.CachingTransformer - Executing the join logic...
```

