/*
 * Copyright 2021 Chad Preisler.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package chad.preisler.kafka.inmemorycache.store;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.ValueAndTimestamp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This transformer will check a client side in memory cache for 
 * records on the right side of a Stream to Table join. 
 * This allows for cyclic relationships where the input KTable
 * on the right uses the same topic as the output of the stream.
 * 
 *    Stream(topicA) join KTable(topicB) to topicB
 * 
 * You must attach a InMemoryStore to the processor context for this to work.
 * You must also attach the KTable to the processor context.
 * @author Chad Preisler
 * @param <K> The key type from left side of join (stream side).
 * @param <L> The value type from the left side of join (stream side)
 * @param <R> The type returned from the right side of join (kTable side)
 */
public class CachingTransformer<K, L, R> implements Transformer<K, L, KeyValue<K, R>>{
    private static final Logger LOGGER = LoggerFactory.getLogger(CachingTransformer.class);
    private final String cacheName;
    private final String ktableName;
    private final JoinLogic<K, L, R> joinLogic;
    private InMemoryStore<K,R> cacheStore;
    private final boolean leftJoin;
    private KeyValueStore<K, ValueAndTimestamp<R>> ktableStore;
    
    public CachingTransformer(String cacheName, String ktableName, boolean leftJoin, JoinLogic<K, L, R> joinLogic) {
        this.cacheName = cacheName;
        this.ktableName = ktableName;
        this.joinLogic = joinLogic;
        this.leftJoin = leftJoin;
    } 
    
    @Override
    public void init(ProcessorContext pc) {
        cacheStore = (InMemoryStore)pc.getStateStore(cacheName);
        ktableStore = (KeyValueStore)pc.getStateStore(ktableName);
    }

    @Override
    public KeyValue<K, R> transform(K key, L leftValue) {
        LOGGER.info("In transformer checking cache for right side.");
        var rValue = cacheStore.get(key);
        if (rValue == null) {
            LOGGER.info("Didn't find it in the cache. Checking KTable.");
            var tableValue = ktableStore.get(key);
            if(tableValue != null) {
                LOGGER.info("Found the value in the KTable.");
                rValue = tableValue.value();
            }
        }
        if (!leftJoin && rValue == null) {
            LOGGER.info("The rValue was not found in the cache or the KTable and not a left join. Returning null.");
            return null;
        }
        LOGGER.info("Executing the join logic...");
        return new KeyValue<>(key, cacheRightSide(key, joinLogic.apply(key, leftValue, rValue)));
    }
    
    private R cacheRightSide(K key, R rValue) {
        if (rValue != null) {
            cacheStore.put(key, rValue);
        }
        return rValue;
    }

    @Override
    public void close() {
        //Stream API will close stores when necessary
    }
}
