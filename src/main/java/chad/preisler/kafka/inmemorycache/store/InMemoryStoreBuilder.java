/*
 * Copyright 2021 Chad Preisler.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package chad.preisler.kafka.inmemorycache.store;

import java.util.Map;
import org.apache.kafka.streams.state.StoreBuilder;

/**
 *
 * @author Chad Preisler
 */
public class InMemoryStoreBuilder<K, V> implements StoreBuilder<InMemoryStore<K, V>> {
    private final String name;
    private final long retentionMs;
    private final Class<K> keyType;
    private final Class<V> valueType;
    
    public InMemoryStoreBuilder(String name, long retentionMs, Class<K> keyType, Class<V> valueType) {
        this.name = name;
        this.retentionMs = retentionMs;
        this.keyType = keyType;
        this.valueType = valueType;
    }
    
    @Override
    public StoreBuilder<InMemoryStore<K, V>> withCachingEnabled() {
        return this;
    }

    @Override
    public StoreBuilder<InMemoryStore<K, V>> withCachingDisabled() {
        return this;
    }

    @Override
    public StoreBuilder<InMemoryStore<K, V>> withLoggingEnabled(Map<String, String> map) {
        return this;
    }

    @Override
    public StoreBuilder<InMemoryStore<K, V>> withLoggingDisabled() {
        return this;
    }

    @Override
    public InMemoryStore<K, V> build() {
        return new InMemoryStore(name, retentionMs, keyType, valueType);
    }

    @Override
    public Map<String, String> logConfig() {
        return null;
    }

    @Override
    public boolean loggingEnabled() {
        return false;
    }

    @Override
    public String name() {
        return name;
    }
    
}
