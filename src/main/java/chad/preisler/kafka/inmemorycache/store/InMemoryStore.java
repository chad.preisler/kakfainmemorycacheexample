/*
 * Copyright 2021 Chad Preisler.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package chad.preisler.kafka.inmemorycache.store;

import java.util.concurrent.TimeUnit;
import org.apache.commons.lang.StringUtils;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.StateStore;
import org.apache.kafka.streams.processor.StateStoreContext;
import org.cache2k.Cache;
import org.cache2k.Cache2kBuilder;

/**
 *
 * @author Chad Preisler
 */
public class InMemoryStore<K,V> implements StateStore {
    boolean open = false;
    private final Cache<K,V> cache;
    private final String name;
    
    public InMemoryStore(String name, long retentionMs, Class<K> keyType, Class<V> valueType) {
        if (StringUtils.isEmpty(name)) {
            throw new RuntimeException("Name of in memory state store can not be null.");
        } 
        this.name = name;
        cache = Cache2kBuilder.of(keyType, valueType).expireAfterWrite(retentionMs, TimeUnit.MILLISECONDS).build();
    }
    
    @Override
    public String name() {
        return name;
    }

    @Override
    public void init(ProcessorContext context, StateStore ss) {
        context.register(this, (k, v)->{});
        this.open = true;
    }

    @Override
    public void flush() {
        // nothing to do
    }

    @Override
    public void close() {
        cache.clear();
        open = false;
    }

    @Override
    public boolean persistent() {
        return false;
    }

    @Override
    public boolean isOpen() {
        return open;
    }
    
    public V get(K key) {
        return cache.get(key);
    }
    
    public void put(K key, V value) {
        cache.put(key, value);
    }
    
}
