/*
 * Copyright 2021 Chad Preisler.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package chad.preisler.kafka.inmemorycache.store;

/**
 * Simple interface that gets the shared key, left side value, and right side 
 * value.
 * @author Chad Preisler
 */
public interface JoinLogic<K, L, R> {

    /**
     * Supplied by application and called by CachingTransformer.
     * @param key The topic key.
     * @param left The value from the left side (stream side) of join.
     * @param right The value from the right side (KTable side) of join.
     * @return Return the new right side value.
     */
    R apply(K key, L left, R right);
}
