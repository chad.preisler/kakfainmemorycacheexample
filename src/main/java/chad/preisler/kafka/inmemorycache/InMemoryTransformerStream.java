/*
 * Copyright 2021 Chad Preisler.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package chad.preisler.kafka.inmemorycache;

import chad.preisler.kafka.inmemorycache.store.CachingTransformer;
import chad.preisler.kafka.inmemorycache.store.InMemoryStoreBuilder;
import chad.preisler.kafka.inmemorycache.store.ZeroTimestampExtractor;
import java.time.Duration;
import java.util.Properties;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Materialized;

/**
 * Stream app that joins stream to KTable using processor API
 * and InMemory cache transformer.
 * 
 * @author Chad Preisler
 */
public class InMemoryTransformerStream {
    
    public static void main (String[] args) {
        Properties config = new Properties();
        config.put(StreamsConfig.APPLICATION_ID_CONFIG, "inMemoryStreamApp");
        config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        config.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        config.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        
        String KTABLE_NAME = "cyclic-b-table";
        String CACHE_NAME = "in-memory-store-b";
        
        StreamsBuilder builder = new StreamsBuilder();
        KStream<String, String> stream = builder.stream("cyclic-a");
        // need to build the table with the zero timestame extractor so it loads
        // like a GlobalKTable otherwise it will start processing before KTable
        // is "fully" loaded.
        builder.table("cyclic-b", Consumed.with(new ZeroTimestampExtractor()), 
                Materialized.as(KTABLE_NAME));
        // attach the in memory cache to the processor
        builder.addStateStore(new InMemoryStoreBuilder(CACHE_NAME, Duration.ofMinutes(2).toMillis(), 
                String.class, String.class));
        
        // "join" the stream to the KTable and write to cyclic-b        
        stream.transform(()-> new CachingTransformer<>(CACHE_NAME, KTABLE_NAME, true,
                (key, lValue, rValue) -> lValue + ", " + rValue), 
                CACHE_NAME, KTABLE_NAME).to("cyclic-b");
        var topology = builder.build();
        System.out.println(topology.describe());
        KafkaStreams streams = new KafkaStreams(topology, config);
        streams.start();
    }
}
