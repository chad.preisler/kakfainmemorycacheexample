/*
 * Copyright 2021 Chad Preisler.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package chad.preisler.kafka.inmemorycache;

import java.util.Properties;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
        

/**
 * Simple producer to cyclic-a
 * @author Chad Preisler
 */
public class ProducerApp {
    public static void main (String[] args) {
        Properties config = new Properties();
        config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        
        var producer = new KafkaProducer<String, String>(config);
        producer.send(new ProducerRecord("cyclic-a", "A", "value-1a"));
        producer.send(new ProducerRecord("cyclic-a", "B", "value-1b"));
        producer.send(new ProducerRecord("cyclic-a", "A", "value-2a"));
        producer.send(new ProducerRecord("cyclic-a", "B", "value-2b"));
        producer.send(new ProducerRecord("cyclic-a", "A", "value-3a"));
        producer.flush();
        producer.close();
    }
}
